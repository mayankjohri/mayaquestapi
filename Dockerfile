FROM python:slim-buster
LABEL maintainer = "Mayank Johri, johrimayank@yandex.com"
WORKDIR /usr/src/app
COPY . /usr/src/app
EXPOSE 5000
COPY install-packages.sh .
RUN ./install-packages.sh
ENTRYPOINT [ "python" ]
CMD [ "app.py"]
